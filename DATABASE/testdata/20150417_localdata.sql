-- MySQL dump 10.13  Distrib 5.5.42, for Linux (x86_64)
--
-- Host: localhost    Database: manabinista
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,'<h1>質問タイトル質問タイトル</h1>','<h1>質問タイトル質問タイトル</h1>','<h1>質問タイトル質問タイトル</h1>',2,NULL,'2015-04-14 19:03:27',NULL),(2,'The standard Lorem Ipsum passage, used since the 1500s','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing ','志賀忠吉',1,'','2015-04-15 19:03:34',NULL),(3,'dsad','dsadsa','dsadsa',2,NULL,'2015-04-16 10:32:17','2015-04-16 10:32:17'),(4,'aa','aa','a',5,NULL,'2015-04-16 13:22:25','2015-04-16 13:22:25'),(5,'dsa','dsa','dsa',1,NULL,'2015-04-16 19:27:23','2015-04-16 19:27:23'),(6,'dsa','dsa','dsa',2,NULL,'2015-04-16 19:38:07','2015-04-16 19:38:07');
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'料理',NULL,NULL,0),(2,'家事',NULL,NULL,0),(3,'手作り',NULL,NULL,0),(4,'住まい',NULL,NULL,0),(5,'キレイ',NULL,NULL,0),(6,'More',NULL,NULL,0);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,1,'<h1>回答回答回答回答</h1>','<h1>回答者</h1>',0,10,'http://placehold.jp/150x150.png','2015-04-14 19:03:51',NULL),(2,1,'いつは当時もうこの排斥人に対して方の時を入れたます。単に今が意味家も断然そんな影響でますだけに加えるていたには発展もっんないば、さっそくには云っなでしだろな。甲に使いた事はいやしくも前によくないたある。\r\nいつは当時もうこの排斥人に対して方の時を入れたます。単に今が意味家も断然そんな影響でますだけに加えるていたには発展もっんないば、さっそくには云っなでしだろな。甲に使いた事はいやしくも前によくないたある。\r\nいつは当時もうこの排斥人に対して方の時を入れたます。単に今が意味家も断然そんな影響でますだけに加えるていたには発展もっんないば、さっそくには云っなでしだろな。甲に使いた事はいやしくも前によくないたある。\r\n','夏目漱石',0,11,NULL,'2015-04-16 19:03:57',NULL),(3,2,'<h1>回答回答回答回答</h1>','',0,NULL,NULL,NULL,NULL),(4,2,'sss','ss',0,NULL,NULL,NULL,NULL),(5,2,'sssss','sdd2222',0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-17 10:18:58
