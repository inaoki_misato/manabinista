<h3>新しい質問投稿する</h3>
<div class="row">
	<?php echo $this->Form->create('Question', array('class' => 'col s12', 'enctype'=>'multipart/form-data')); ?>
		<div class="row">
			<div class="input-field col s12">
				<?php echo $this->Form->input('title', array(
					'length' => '30',
					'label'  => '質問タイトル'
				)); ?>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<?php echo $this->Form->textarea('message', array(
					'length' => '450',
					'rows'   => '5',
					'class'  => 'materialize-textarea'
				))?>
				<label for="textarea">質問内容</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<?php echo $this->Form->input('username', array(
					'length' => '20',
					'label'  => 'お名前'
				))?>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<label>カテゴリ</label>
				<?php echo $this->Form->select('category_id', $categories, array(
						'class' => 'browser-default'
				))?>
			</div>
		</div>
		<div class="row">
			<div class="col s12 file-field input-field">
				<?php echo $this->Form->input('image', array(
					'type'     => 'text',
					'label'    => false,
					'error'    => false,
					'required' => false,
					'class'    => 'file-path validate'
				))?>
				<div class="btn">
					<span>画像</span>
					<?php echo $this->Form->input('file', array('type'=>'file', 'label'=> false)); ?>
				</div>
				<?php echo $this->Form->error('Question.image', null, array('wrap' => 'span', 'class' => 'red-text text-darken-2')); ?>
			</div>
		</div>
		<?php echo $this->Form->button(
			'送信する<i class="mdi-content-send right"></i>', array(
			'class' => 'btn waves-effect waves-light',
			'type'  => 'submit',
			'id'    => 'submit'
		)); ?>
		<?php echo $this->Form->input('sessionTimeStamp', array(
			'type'  => 'hidden',
			'value' => $sessionTimeStamp
		)) ?>
	<?php echo $this->Form->end(); ?>
</div>