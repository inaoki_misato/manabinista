<?php
	echo $this->Form->isFieldError('Answer');
	echo $this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
	if (<?php echo $modalFlg?>) {
		$('#modal1').openModal();
	}
});
<?php
	echo $this->Html->scriptEnd();
//}?>
<div id="modal1" class="modal bottom-sheet  modal-fixed-footer">
	<div class="modal-content">
	<?php echo $this->Form->create('Answer', array('class' => 'col s12', 'type' => 'file')); ?>
		<div class="row">
			<div class="input-field col s12">
				<?php echo $this->Form->textarea('message', array(
					'length' => '450',
					'rows'   => '8',
					'class'  => 'materialize-textarea')
				)?>
				<label for="textarea">回答</label>
				<?php echo $this->Form->error('Answer.message', null, array('wrap' => 'span', 'class' => 'red-text text-darken-2')); ?>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<?php echo $this->Form->input('username', array(
					'length' => '20',
					'label'  => 'お名前',
					'error'  => false
				))?>
				<?php echo $this->Form->error('Answer.username', null, array('wrap' => 'span', 'class' => 'red-text text-darken-2')); ?>
			</div>
		</div>
		<div class="row">
			<div class="col s12 file-field input-field">
				<?php echo $this->Form->input('image', array(
					'type'     => 'text',
					'label'    => false,
					'error'    => false,
					'required' => false,
					'class'    => 'file-path validate'
				))?>
				<div class="btn">
					<span>画像</span>
					<?php echo $this->Form->input('file', array('type' => 'file', 'label'=> false, 'error' => false, 'required' => false))?>
				</div>
				<?php echo $this->Form->error('Answer.image', null, array('wrap' => 'span', 'class' => 'red-text text-darken-2')); ?>
			</div>
		</div>
		<?php
			echo $this->Form->button(
				'送信する<i class="mdi-content-send right"></i>', array(
					'class' => 'btn waves-effect waves-light',
					'type'  => 'submit',
					'id'    => 'submit'
				)
			);
			echo $this->Form->input('sessionTimeStamp', array(
				'type'  => 'hidden',
				'value' => $sessionTimeStamp
			));
			echo $this->Form->end();
		?>
	</div>
	<div class="modal-footer">
		<a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">キャンセル</a>
	</div>
</div>
<div class="section">
	<p>
	<?php
		$this->Html->addCrumb($question['Category']['name'], "/?category_id={$question['Question']['category_id']}");
		echo $this->Html->getCrumbs(' > ', 'HOME');
	?>
	</p>
	<div class="card">
		<div class="card-content">
			<p class="grey-text darken-1"><?php echo $this->Time->format($question['Question']['created'], '%Y年%m月%d日 %g時%m分')?>に質問</p>
			<span class="card-title grey-text text-darken-4"><?php echo h($question['Question']['title'])?></span>
			<p><?php echo nl2br(h($question['Question']['message']))?></p>
			<p>
			<?php if (!empty($question['Question']['image'])) {
				echo $this->Html->image("../files/" . $question['Question']['image']);
			}?>
			</p>
			<p class="right-align"><?php echo h($question['Question']['username'])?>さんより</p>
		</div>
		<div class="card-action">
			<a class="modal-trigger" href="#modal1">回答する</a>
		</div>
	</div>
</div>

<div class="section">
	<?php if (!empty($answers)) {?>
	<h3>回答一覧</h3>
	<?php
		foreach ($answers as $key => $data) {
			if (!$data['Answer']['reply_flg']) {
	?>
	<div class="card">
		<div class="card-content">
			<p class="grey-text darken-1"><?php echo $this->Time->format($data['Answer']['created'], '%Y年%m月%d日 %g時%m分')?>に回答</p>
			<p><?php echo h($data['Answer']['message'])?></p>
			<p><?php if(!empty($data['Answer']['image'])) {
				echo $this->Html->image("../files/" . $data['Answer']['image']);
			}?></p>
			<p class="right-align"><?php echo h($data['Answer']['username'])?>さんより</p>
		</div>
	</div>
	<?php
			}
		}
	} else {?>
	<h3 class="center-align">回答はありません</h3>
	<p class="center-align">回答してみませんか？<br>
	<a class="btn btn-large waves-light waves-effect modal-trigger" href="#modal1"><i class="mdi-content-create left"></i>回答する</a>
	</p>
	<?php }?>
</div>