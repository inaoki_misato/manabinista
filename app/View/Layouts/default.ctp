<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

 $site_name = 'MANABINISTA';
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
	<?php echo $site_name ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('materialize.min.css');
		echo $this->Html->script('jquery.min.js');
		echo $this->Html->script('materialize.min.js');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<script type="text/javascript">
		$(document).ready(function(){
			// モーダルウィンドウ読み込み
			$('.modal-trigger').leanModal();
			$('.tooltipped').tooltip({delay: 50});

			// ダブルクリックによるフォーム誤送信を防ぐ
			$('[type=submit]').parent('form').on('submit', function() {
				$(this).children('[type=submit]').attr('disabled', 'disabled');
			})
		});
	</script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
</head>
<body>
	<div id="container">
		<nav>
			<div class="nav-wrapper">
				<?php echo $this->Html->link(
					$site_name,
					array('controller' => 'pages'),
					array('class' => 'brand-logo')
				); ?>
			</div>
		</nav>
		<div id="content" class="row">
			<div class="col l8 s12">
				<?php echo $this->session->flash('flash', array('element' => 'error')); ?>
				<?php echo $this->fetch('content'); ?>
			</div>
			<div class="col l4 s12 hide-on-small-only">
				<?php echo $this->element('most_answers_question', array('cache' => '+10 hour')); ?>
			</div>
		</div>
	</div>
</body>
</html>
