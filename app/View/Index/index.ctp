<div class="section">
<?php
	echo $this->Html->link(
		'<i class="mdi-image-edit left"></i> 投稿する',
		array(
			'controller' => 'posts',
			'action'     => 'index'
		),
		array(
			'class'  => 's12 white-text waves-effect waves-light btn-large btn-flat orange darken-1',
			'escape' => false
	));
?>
</div>

<div class="section">
<h3>カテゴリ一覧</h3>
<?php
	foreach ($categories as $id => $category) {
		echo $this->Html->link(
			$category,
			array(
				'controller' => 'index',
				'action'     => 'index',
				'?' => array(
					'category_id' => $id
				)
			),
			array(
				'class'  => 'waves-effect btn-flat grey lighten-2',
				'escape' => false
		)); 
	}
?>
</div>

<div class="section">
<h3>
<?php
	if ($this->request->query) {
		$id = $this->request->query['category_id'];
		echo h($categories[$id]) . "に関する質問";
	} else {
		echo '最新の質問';
	}
?>
</h3>
<ul class="collection">
	<?php foreach ($questions as $id => $list) {?>
	<li class="collection-item">
		<?php
			echo $this->Html->link(
				$list['Question']['title'],
				array(
					'controller' => 'posts',
					'action'     => 'view',
					$list['Question']['id']
				)
			)
		?>
		(<?php echo h($list['Question']['answers_count'])?>)
		<span class="badge">
			[<?php echo h($list['Category']['name'])?>] 
			<?php echo h($list['Question']['created'])?>
		</span>
	</li>
	<?php }?>
</ul>
<ul class="pagination">
	<li class="<?php echo ($this->paginator->hasPrev()) ? 'waves-effect' : 'disabled'?>">
		<?php
			echo $this->Paginator->prev(
				'<i class="mdi-navigation-chevron-left"></i>',
				array('escape' => false),
				null,
				array('tag' => 'a')
			);
			echo $this->Paginator->first(3, array('tag' => 'li.waves-effect'));
		?>
	</li>
	<li class="active">
		<a href="#!"><?php echo $this->Paginator->current()?></a>
	</li>
	<li class="<?php echo ($this->paginator->hasNext()) ? 'waves-effect' : 'disabled'?>">
	<?php 
		echo $this->Paginator->next(
			'<i class="mdi-navigation-chevron-right"></i>',
			array('escape' => false),
			null,
			array('tag' => 'a')
		);
	?>
	</li>
</ul>
</div>