<?php $list = $this->requestAction('/get_most_answers_question'); ?>
<h3>回答の多い質問</h3>
<ul class="collection">
	<?php foreach ($list as $key => $value) { ?>
	<li class="collection-item row">
		<?php echo $this->Html->link($value['Question']['title'], array(
			'controller' => 'posts',
			'action'     => 'view',
			$value['Question']['id']
		)); ?>
		(<?php echo h($value['Question']['answers_count']); ?>)<br>
		[<?php echo h($value['Category']['name']); ?>]<?php echo h($value['Question']['created']); ?>
	</li>
	<?php } ?>
</ul>
