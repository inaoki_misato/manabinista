<?php
App::uses('AppModel', 'Model');

class Answer extends AppModel {
	var $name = 'Answer';

	var $validate = array(
		'message' => array(
			'rule'       => array('maxLength', LIMIT_ANSWER_MESSAGE_NUMBERS),
			'required'   => true,
			'allowEmpty' => false,
			'message'    => '回答内容を450文字以内で投稿して下さい。'
		),
		'username' => array(
			'rule'       => array('maxLength', LIMIT_NAME_NUMBERS),
			'required'   => true,
			'allowEmpty' => false,
			'message'    => 'お名前を20文字以内で投稿して下さい。'
		),
		'image' => array(
			'rule'       => '/\.(jpg|jpeg|png|gif)$/i',
			'required'   => false,
			'allowEmpty' => true,
			'message'    => 'アップロードできるのはJPEG, PNG, GIFデータのみです。',
		)
	);

	/**
	 * 質問IDから、回答データを取得する
	 * getAnswers
	 *
	 * @param  $id questionId
	 * @return array
	 */
	public function getAnswers($id)
	{
		$param = array(
			'fields'=> array(
				'id',
				'username',
				'message',
				'reply_flg',
				'votes',
				'image',
				'created'
			),
			'order' => array(
				'created desc'
			),
			'conditions' => array(
				'question_id' => $id
			)
		);
		$data = $this->find('all', $param);

		return $data;
	}
}