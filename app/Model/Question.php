<?php
App::uses('AppModel', 'Model');

/**
 * Question Model
 *
 */
class Question extends AppModel {
	var $name = 'Question';

	// CategoryとQuestionをつなぐ
	var $belongsTo = array(
		'Category' => array(
			'className'  => 'Category',
			'foreignKey' => 'category_id',
			'dependent'  => false
		)
	);
	var $validate = array(
		'title' => array(
			'rule' => array('maxLength', LIMIT_QUESTION_TITLE_NUMBERS),
			'required'   => true,
			'allowEmpty' => false,
			'message'    => '質問タイトルを30文字以内で投稿して下さい。'
		),
		'message' => array(
			'rule'       => array('maxLength', LIMIT_QUESTION_MESSAGE_NUMBERS),
			'required'   => true,
			'allowEmpty' => false,
			'message'    => '質問内容を450文字以内で投稿して下さい。'
		),
		'username' => array(
			'rule'       => array('maxLength', LIMIT_NAME_NUMBERS),
			'required'   => true,
			'allowEmpty' => false,
			'message'    => 'お名前を20文字以内で投稿して下さい。'
		),
		'category_id' => array(
			'rule'       => 'naturalNumber',
			'required'   => true,
			'allowEmpty' => false,
			'message'	   => 'カテゴリー 不正な入力です。'
		),
	// 'image'=>array(
	// 		'rule1' => array(
	// 			//拡張子の指定
	// 			'rule' => array('extension',array('jpg','jpeg','gif','png')),
	// 			'message' => '画像ではありません。',
	// 			'allowEmpty' => true,
	// 		),
	// 		'rule2' => array(
	// 			//画像サイズの制限
	// 			'rule' => array('fileSize', '<=', '500000'),
	// 			'message' => '画像サイズは500KB以下でお願いします',
	// 		)
	//  ),
	);

	/**
	 * getAllList
	 * 回答数とカテゴリ名を合わせた質問一覧を取得
	 *
	 * @return array $data 質問データ
	 */
	public function getAllList($limit = 5, $order = array('created DESC'))
	{
		// answers_countに回答数を格納
		$this->virtualFields['answers_count'] = 'SELECT COUNT(*) FROM answers WHERE question_id = Question.id';

		$param = array(
			'fields'=> array(
				'id',
				'title',
				'username',
				'answers_count',
				'category_id',
				'Category.name',
				'image',
				'created'
			),
			'limit' => $limit,
			'order' => $order
		);

		$data = $this->find('all', $param);
		return $data;
	}

	/**
	 * getQuestion
	 * 質問IDの質問を取得
	 *
	 * @param int $id QuestionId
	 * @return array $data 質問データ
	 */
	public function getQuestion($id)
	{
		$param = array(
			'fields'=> array(
				'id',
				'title',
				'message',
				'username',
				'category_id',
				'Category.name',
				'image',
				'created'
			),
			'conditions' => array(
				'Question.id' => $id
			)
		);

		$data = $this->find('first', $param);

		return $data;
	}
}