<?php
App::uses('AppModel', 'Model');

/**
 * Category Model
 *
 */
class Category extends AppModel {
	var $name = 'Category';
	/**
	 * カテゴリIDとカテゴリ一覧を取得する
	 * getAllList
	 * @param  $category_id カテゴリID
	 * @return String
	 */
	public function getAllList() {
		$param = array(
			'fields'=>array(
				'name'
			),
			'conditions' => array()
		);

		$data = $this->find('list', $param);

		return $data;
	}
	
}