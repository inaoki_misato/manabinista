<?php
App::uses('AppController', 'Controller');

class IndexController extends AppController {

	public $uses = array('Question', 'Category');
	public $components = array('Paginator');

	public $paginate = array(
		'Question' => array(
			'maxLimit' => LIMIT_QUESTION_LIST,
			'contain'  => array(
				'id',
				'title',
				'answers_count',
				'Category.name',
				'Category.id',
				'image',
				'created'
			),
			'order' => array(
				'created' => 'desc'
			)
		)
	);

	/**
	 * index
	 * カテゴリ別リンクと、新着一覧を表示
	 */
	public function index() {
		// URLにパラメータ「category_id」が指定されていた場合はカテゴリ別表示
		if ($this->request->query) {
			$id = $this->request->query['category_id'];
			$this->paginate['Question']['conditions'] = array('Category.id' => $id);
		}
		// Paginatorの設定をセット
		$this->Paginator->settings = $this->paginate;
		// 各質問に回答数をセット
		$this->Question->virtualFields['answers_count'] = 'SELECT COUNT(*) FROM answers WHERE question_id = Question.id';

		$categories = $this->Category->getAllList();
		$questions = $this->Paginator->paginate('Question');
		$this->set('categories', $categories);
		$this->set('questions', $questions);

		return;
	}
}