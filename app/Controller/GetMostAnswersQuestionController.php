<?php
App::uses('AppController', 'Controller');

/**
 * GetMostAnswersQuestion Controller
 * 回答数の多い順に質問を取得します。
 * 回答数が同じ場合は投稿日時の新しい順にソートします。
 *
 */
class GetMostAnswersQuestionController extends AppController {

	/**
	 * index
	 *
	 * @return array 質問リスト
	 */
	public function index() {

		$this->loadModel('Question');
		$list = $this->Question->getAllList(
			LIMIT_MOST_ANSWERS_QUESTION_LIST,
			array('answers_count DESC', 'created DESC')
		);

		return $list;
	}
}
