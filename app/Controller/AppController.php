<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('Security', 'Utility');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	/**
	 * checkCSRF
	 *
	 * @param $key
	 * @param int $msgId
	 * @param string $url
	 * @return mixed
	 * @throws NotFoundException
	 * @author thien.nguyen
	 */
	public function checkCSRF($key, $msgId = 1, $url = '') {
		$msg = array(
			'1' => '処理の途中で失敗しました。恐れ入りますが、もう一度お試しください。',
		);

		if (!$this->Session->check($key)) {
			$stack = debug_backtrace();
			if (isset($stack[0]['file']) && $stack[0]['line']) {
				$file = "\nFilename: " . $stack[0]['file'];
				$line = "\nLine: " . $stack[0]['line'];
				$this->log('ERROR:' . $msg[$msgId] . $file . $line, 'error');
			}
			if ($url == '') {
				throw new NotFoundException($msg[$msgId]);
			} else if ($url == null) {
				return;
			} else {
				$this->redirect($url);
				return;
			}
		}

		$value = $this->Session->read($key);
		$this->Session->delete($key);
		return $value;
	}

	/**
	 * makeCSRF
	 *
	 * @param $key
	 * @author thien.nguyen
	 */
	public function makeCSRF($key) {
		$sessionTimeStamp = Security::hash(date('Y-m-dH:i:s') . rand(5, 15), 'sha1', true);
		$this->Session->write($key . $sessionTimeStamp, 1);
		$this->set('sessionTimeStamp', $sessionTimeStamp);
	}
}
