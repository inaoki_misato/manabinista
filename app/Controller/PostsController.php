<?php

/**
 * Posts Controller
 * 質問の追加、表示を行います。
 *
 */
class PostsController extends AppController {

	public $uses = array('Question', 'Category','Answer');

	/**
	 * index Controller
	 * 質問の投稿画面を表示します。
	 */
	public function index() {
		if ($this->request->is('post')) {
			// リクエストが飛んできたら投稿する
			$this->__add();
		}
		$this->makeCSRF(SESSION_TIMESTAMP_POST_ARTICLE);
		$data = $this->Category->getAllList();
		$this->set('categories', $data);

		return;
	}

	/**
	 * __add Controller
	 * 質問の投稿画面を表示します。
	 *
	 * @throws NotFoundException データの保存に失敗した場合
	 */
	private function __add() {
		$this->Question->create();
		$data = $this->request->data;

		$this->checkCSRF(SESSION_TIMESTAMP_POST_ARTICLE . $data['Question']['sessionTimeStamp']);

		// 画像のファイル名を設定
		if(!empty($data['Question']['file']['name'])) {
			$image = $data['Question']['file'];
			$data['Question']['image']  = date('Ymd-His') . '.' .  pathinfo($image['name'], PATHINFO_EXTENSION);
		}

		unset($data['Question']['file']);
		// Save
		if ($this->Question->save($data)) {
			if(isset($image)) {
				move_uploaded_file($image['tmp_name'], WWW_ROOT . 'files' . DS . $data['Question']['image']);
			}
			$id = $this->Question->getLastInsertID();
			$this->redirect(array('action' => 'view', $id));
		} else {
			$this->Session->setFlash('投稿できませんでした。');
		}
	}

	/**
	 * view Controller
	 * 質問の個別閲覧画面を表示します。
	 *
	 * @param int $id QuestionId
	 */
	public function view($id = null) {
		// パラメータ無ければトップへ戻る
		if (is_null($id)) {
			$this->redirect(array('controller' => 'index'));
		}

		if ($this->request->is('post')) {
			// リクエストが飛んできたら投稿する
			$this->__addAnswer($id);
		}
		$answers = $this->Answer->getAnswers($id);
		$question = $this->Question->getQuestion($id);

		$this->set('question', $question);
		$this->set('answers', $answers);

		$this->makeCSRF(SESSION_TIMESTAMP_POST_ARTICLE);

		return;
	}

	/**
	 * __addAnswer Controller
	 * 回答を投稿します。
	 *
	 * @param int $id QuestionId
	 * @throws NotFoundException データの保存に失敗した場合
	 */
	private function __addAnswer($id = null) {
		if (is_null($id)) {
			return;
		}

		$this->Answer->create();
		$data = $this->request->data;

		$this->checkCSRF(SESSION_TIMESTAMP_POST_ARTICLE . $data['Answer']['sessionTimeStamp']);

		// 画像のファイル名を設定
		if(!empty($data['Answer']['file']['name'])) {
			$image = $data['Answer']['file'];
			$data['Answer']['image']  = date('Ymd-His') . '.' .  pathinfo($image['name'], PATHINFO_EXTENSION);
		}

		unset($data['Answer']['file']);
		$data['Answer']['question_id'] = $id;
		// Save
		if ($this->Answer->save($data)) {
			if(isset($image)) {
				move_uploaded_file($image['tmp_name'], WWW_ROOT . 'files' . DS . $data['Answer']['image']);
			}
			$this->redirect($this->referer());
		} else {
			$this->set('modalFlg', true); // Modalウィンドウを表示させる
			$this->Session->setFlash('投稿できませんでした。');
		}
	}
}