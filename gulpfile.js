var gulp = require('gulp');
var browserSync = require('browser-sync');

gulp.task('browser-sync', function () {
	browserSync({
		proxy: "localhost"
	});
});

gulp.task('reload', function () {
	browserSync.reload();
});

gulp.task('default', ['browser-sync'], function () {
	gulp.watch(["app/**/*.php","app/**/*.ctp","app/**/*.js","app/**/*.css"], ['reload']);
});